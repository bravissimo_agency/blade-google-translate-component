<google-translate-provider 
    :included-languages='<?php echo json_encode($includedLanguages, 15, 512) ?>'
    page-language="<?php echo e($pageLanguage); ?>"
    v-slot:default="GoogleTranslateProvider"
    <?php echo e($attributes->only(['@changed'])); ?>

>
    <div <?php echo e($attributes->except(['@changed'])); ?>>
        <div class="custom-translate" style="display: none;" id="google_translate_element"></div>

        <?php echo $slot; ?>

    </div>
</google-translate-provider><?php /**PATH C:\laragon\www\blade-google-translate-component\resources/views/googleTranslate.blade.php ENDPATH**/ ?>