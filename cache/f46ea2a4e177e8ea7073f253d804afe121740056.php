<?php $attributes = $attributes->exceptProps([
    'langCode',
    'activeClass' => 'isActive'
]); ?>
<?php foreach (array_filter(([
    'langCode',
    'activeClass' => 'isActive'
]), 'is_string', ARRAY_FILTER_USE_KEY) as $__key => $__value) {
    $$__key = $$__key ?? $__value;
} ?>
<?php $__defined_vars = get_defined_vars(); ?>
<?php foreach ($attributes as $__key => $__value) {
    if (array_key_exists($__key, $__defined_vars)) unset($$__key);
} ?>
<?php unset($__defined_vars); ?>

<button
    type="button"
    v-bind:class="{ '<?php echo e($activeClass); ?>': GoogleTranslateProvider.activeLang === '<?php echo e($langCode); ?>' }"
    @click="GoogleTranslateProvider.$changeLang('<?php echo e($langCode); ?>')"
    <?php echo e($attributes); ?>

>
    <?php echo $slot; ?>

</button><?php /**PATH C:\laragon\www\blade-google-translate-component\resources/views/googleTranslateButton.blade.php ENDPATH**/ ?>