 <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'layouts.app','data' => ['class' => 'p-10 flex flex-wrap']]); ?>
<?php $component->withName('app'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['class' => 'p-10 flex flex-wrap']); ?>
    <div class="w-1/2 p-1">
         <?php if (isset($component)) { $__componentOriginala94379fc5bfbfd56051eb7cfcc39c195f461b97d = $component; } ?>
<?php $component = $__env->getContainer()->make(Bravissimo\BladeGoogleTranslateComponent\View\Components\GoogleTranslate::class, ['pageLanguage' => 'en','includedLanguages' => ['en', 'sv', 'it'],'ignoredLanguages' => ['ceb', 'ny', 'ka', 'fy', 'ha', 'haw', 'ig', 'la', 'kk', 'mi', 'xh', 'si', 'or', 'lo', 'hmn', 'yo', 'te']]); ?>
<?php $component->withName('googleTranslate'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['@changed' => 'log']); ?>
            <?php $__currentLoopData = $component->getLanguages(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                 <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'views.googleTranslateButton','data' => ['langCode' => $key,'class' => 'googleTranslateButton skiptranslate']]); ?>
<?php $component->withName('googleTranslateButton'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['lang-code' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute($key),'class' => 'googleTranslateButton skiptranslate']); ?>
                    <img
                        src="/resources/assets/<?php echo e($key); ?>.png"
                        style="width: 18px; height: 12px;"
                    >

                    <?php echo e($language); ?>

                 <?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         <?php if (isset($__componentOriginala94379fc5bfbfd56051eb7cfcc39c195f461b97d)): ?>
<?php $component = $__componentOriginala94379fc5bfbfd56051eb7cfcc39c195f461b97d; ?>
<?php unset($__componentOriginala94379fc5bfbfd56051eb7cfcc39c195f461b97d); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>  
    </div>

    <p>
        MY text
    </p>
 <?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> <?php /**PATH C:\laragon\www\blade-google-translate-component\examples/example.blade.php ENDPATH**/ ?>