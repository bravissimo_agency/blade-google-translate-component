<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>blade-accordion-component</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <?php if(!isHmrMode()): ?>
            <link rel="stylesheet" href="<?php echo e(mix('app.css')); ?>">
            <script src="<?php echo e(mix('app.js')); ?>" defer></script>
        <?php else: ?>
            <script src="<?php echo e(mix('app.js')); ?>" defer></script>
        <?php endif; ?>
    </head>

    <body>
        <div id="app" <?php echo e($attributes); ?>>
            <?php echo $slot; ?>

        </div>
    </body>
</html>
<?php /**PATH C:\laragon\www\blade-google-translate-component\examples/layouts/app.blade.php ENDPATH**/ ?>