<google-translate-provider v-slot:default="GoogleTranslateProvider">
    <div>
        <div class="custom-translate" style="display: none;" id="google_translate_element"></div>

        <button
            type="button"
            v-bind:class="{ 'isActive': GoogleTranslateProvider.activeLang === 'en' }"
            class="googleTranslateButton"
            @click="GoogleTranslateProvider.$changeLang('en')"
        >
            Engelska
        </button>

        <button
            type="button"
            v-bind:class="{ 'isActive': GoogleTranslateProvider.activeLang === 'sv' }"
            class="googleTranslateButton"
            @click="GoogleTranslateProvider.$changeLang('sv')"
        >
            Svenska
        </button>

        <button
            type="button"
            v-bind:class="{ 'isActive': GoogleTranslateProvider.activeLang === 'de' }"
            class="googleTranslateButton"
            @click="GoogleTranslateProvider.$changeLang('de')"
        >
            Tyska
        </button>
    </div>
</google-translate-provider><?php /**PATH C:\laragon\www\blade-google-translate-component\src/components/googleTranslate.blade.php ENDPATH**/ ?>