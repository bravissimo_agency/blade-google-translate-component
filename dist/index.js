const components = {
    GoogleTranslateProvider: () =>
        import(
            /* webpackChunkName: "google-translate-provider" */ './GoogleTranslateProvider-66ab478c.js'
        )
};

var index = Vue => {
    for (const name in components) {
        Vue.component(name, components[name]);
    }
};

export default index;
