import Cookies from 'js-cookie';

const CALLBACK_NAME = 'googleTranslateInit';

let initialized = false;
let resolveInitPromise;
let rejectInitPromise;

const initPromise = new Promise((resolve, reject) => {
    resolveInitPromise = resolve;
    rejectInitPromise = reject;
});

function init () {
    if (initialized) {
        return initPromise;
    }

    initialized = true;

    window[CALLBACK_NAME] = () => resolveInitPromise(window.google);

    const script = document.createElement('script');
    script.async = true;
    script.defer = true;
    script.src = `https://translate.google.com/translate_a/element.js?cb=${CALLBACK_NAME}`;
    script.onerror = rejectInitPromise;
    document.querySelector('head').appendChild(script);

    return initPromise;
}

var script = {
    props: {
        pageLanguage: {
            type: String,
            required: true
        },

        includedLanguages: {
            type: Array,
            default: null
        }
    },

    data: () => ({
        activeLang: null
    }),

    async mounted () {
        const google = await init(this.apiKey);

        // eslint-disable-next-line
        new google.translate.TranslateElement(
            {
                pageLanguage: this.pageLanguage,
                includedLanguages: this.includedLanguages
                    ? this.includedLanguages.join(',')
                    : null,
                autoDisplay: false
            },
            'google_translate_element'
        );

        this.activeLang =
            Cookies.get('GoogleAccountsLocale_session') || this.pageLanguage;
    },

    methods: {
        changeLang (lang) {
            this.$emit('lang-changed', lang);

            this.activeLang = lang;

            if (this.pageLanguage === lang) {
                Cookies.remove('googtrans', { path: '' });
                Cookies.remove('googtrans', {
                    path: '',
                    domain: `.${window.location.hostname}`
                });
            } else {
                Cookies.set('googtrans', `/${this.pageLanguage}/${lang}`, {
                    expires: 999,
                    path: ''
                });
            }

            Cookies.set('GoogleAccountsLocale_session', lang, {
                expires: 999
            });

            this.$nextTick(() => {
                const el = document.querySelector(
                    '#google_translate_element select'
                );

                el.value = lang;
                el.dispatchEvent(new Event('change'));
                el.dispatchEvent(new Event('change'));
            });
        }
    },

    render () {
        return this.$scopedSlots.default({
            $changeLang: this.changeLang,
            activeLang: this.activeLang
        });
    }
};

export default script;
