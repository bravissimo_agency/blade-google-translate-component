const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');
const path = require('path');
const cssvariables = require('postcss-css-variables');
const atImport = require('postcss-import');
require('laravel-mix-purgecss');
require('laravel-mix-blade-reload');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

const distFolder = path.resolve(__dirname) + '/dist';

mix.setResourceRoot('/')
    .setPublicPath('./')
    .options({
        extractVueStyles: true,
        fileLoaderDirs: {
            fonts: 'dist/fonts',
            images: 'dist/images'
        }
    })
    .js('examples/app.js', distFolder)
    .postCss('examples/app.css', distFolder, [
        atImport({
            filter: file => {
                return !file.includes('tailwind');
            }
        }),
        cssvariables({
            preserveAtRulesOrder: true
        }),
        require('postcss-preset-env')({
            stage: 0,
            preserve: false,
            features: {
                'custom-properties': false
            }
        }),
        require('postcss-calc')(),
        tailwindcss('./tailwind.config.js')
    ])
    .purgeCss({
        whitelist: ['home'],
        whitelistPatterns: [
            /-active$/,
            /-enter$/,
            /-leave-to$/,
            /show$/,
            /tns/,
            /^pswp/
        ]
    });

if (mix.inProduction()) {
    mix.version();

    mix.webpackConfig({
        output: {
            chunkFilename: '[name].js?id=[chunkhash]',
            publicPath: '/'
        }
    });

    mix.extract(['vue']);
} else {
    mix.bladeReload({
        path: 'examples/**/**/*.blade.php',
        debug: false
    }).sourceMaps();
}

mix.override(config => {
    config.module.rules.push({
        test: /\.vue$/,
        use: [
            {
                loader: 'vue-svg-inline-loader',
                options: {
                    svgo: false
                }
            }
        ]
    });
});

mix.babelConfig({
    plugins: ['@babel/plugin-syntax-dynamic-import']
});

mix.webpackConfig({
    resolve: {
        alias: {
            '~': path.resolve(__dirname),
            '@': path.resolve('examples/')
        }
    },
    module: {
        rules: [
            {
                test: /\.postcss$/,
                use: ['style-loader', 'css-loader', 'postcss-loader'],
                include: path.resolve(__dirname, '../')
            },
            {
                enforce: 'pre',
                test: /\.(js|vue)$/,
                loader: 'eslint-loader',
                exclude: /node_modules/
            },
            {
                test: /\.m?js$/,
                exclude: /node_modules\/(?!unfetch\/)/,
                use: 'babel-loader'
            }
        ]
    }
});
