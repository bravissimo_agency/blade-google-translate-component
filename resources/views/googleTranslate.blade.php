<google-translate-provider 
    :included-languages='@json($includedLanguages)'
    page-language="{{ $pageLanguage }}"
    v-slot:default="GoogleTranslateProvider"
    {{ $attributes->only(['@changed']) }}
>
    <div {{ $attributes->except(['@changed']) }}>
        <div class="custom-translate" style="display: none;" id="google_translate_element"></div>

        {!! $slot !!}
    </div>
</google-translate-provider>