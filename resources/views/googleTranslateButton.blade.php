@props([
    'langCode',
    'activeClass' => 'isActive'
])

<button
    type="button"
    v-bind:class="{ '{{ $activeClass }}': GoogleTranslateProvider.activeLang === '{{ $langCode }}' }"
    @click="GoogleTranslateProvider.$changeLang('{{ $langCode }}')"
    {{ $attributes }}
>
    {!! $slot !!}
</button>