const components = {
    GoogleTranslateProvider: () =>
        import(
            /* webpackChunkName: "google-translate-provider" */ './GoogleTranslateProvider.vue'
        )
};

export default Vue => {
    for (const name in components) {
        Vue.component(name, components[name]);
    }
};
