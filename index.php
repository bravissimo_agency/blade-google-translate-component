<?php

require_once './vendor/autoload.php';

use Jenssegers\Blade\Blade;

class App extends \Illuminate\Container\Container {
    public function getNamespace() {
        return 'App';
    }
}

$views = ['examples', 'resources'];
$cache = 'cache';

$app = \Illuminate\Container\Container::getInstance();

$blade = new Blade($views, $cache, new App);

$app->bind(
    'view',
    function ($app) use ($blade) {
        return $blade;
    }
);

$app->bind(
    'Illuminate\Contracts\View\Factory',
    function ($app) use ($blade) {
        return $blade;
    }
);

$app->bind(
    'blade.compiler',
    function ($app) use ($blade) {
        return $blade->compiler();
    }
);

$app->bind(
    'view.compiled',
    function ($app) use ($blade) {
        return $blade->container->get('config')['view.compiled'];
    }
);

$app->bind(
    'config',
    function ($app) use ($views, $cache) {
        return new Config($views, $cache);
    }
);
class Config {
    public $attributes;
    public $cache;

    public function __construct($views, $cache) {
        $this->attributes = [
            'view.paths' => $views,
            'view.compiled' => $cache,
        ];
    }

    public function get($name) {
        return $this->attributes[$name];
    }
}

function mix(?string $type = null) : ?string {
    $assets = file_get_contents('mix-manifest.json');
    $assets = json_decode($assets, true);

    if (true) {
        $url = file_get_contents('hot');

        return $url . '/dist/' . $type;
    }

    foreach ($assets as $key => $asset) {
        if (Str::endsWith($key, $type)) {
            return $asset;
        }
    }
}

function isHmrMode() {
    return file_exists('hot');
}

function getSvg(string $name, $class = '') {
    $svg = file_get_contents('resources/assets/images/' . $name . '.svg');

    if (empty($class)) {
        return $svg;
    }

    $doc = new \DOMDocument;
    $doc->loadXML($svg);
    $svgs = $doc->getElementsByTagName('svg');

    foreach ($svgs as $svg) {
        $svg->setAttribute('class', $class);
    }

    return $doc->saveHTML();
}

function svgBladeDirective($expression) {
    return "<?php echo getSvg($expression) ?>";
}

$app->bind(
    'Illuminate\Contracts\Foundation\Application',
    function () {
        return \Illuminate\Support\Facades\Facade::getFacadeApplication();
    }
);

$blade->compiler()->components([
    'layouts.app' => 'app',
    'googleTranslate' => \Bravissimo\BladeGoogleTranslateComponent\View\Components\GoogleTranslate::class,
    'views.googleTranslateButton' => 'googleTranslateButton',
]);

$blade->directive('svg', 'svgBladeDirective');

//echo (new \Bravissimo\BladeGoogleTranslateComponent\View\Components\GoogleTranslate)->render();

echo $blade->render('example', ['name' => 'John Doe']);
