const plugin = require('tailwindcss/plugin');

module.exports = {
    theme: {
        colors: {
            primary: '#90cdf4',
            secondary: '#e6003c',
            white: '#fff',
            black: '#000',
            gray: '#f4f4f4'
        },

        spacing: {
            0: 0,
            1: '1rem',
            2: '2rem',
            3: '3rem',
            4: '4rem',
            5: '5rem',
            6: '6rem',
            7: '7rem',
            8: '8rem',
            9: '9rem',
            10: '10rem',
            20: '20rem',
            30: '30rem',
            40: '40rem',
            50: '50rem',
            60: '60rem',
            70: '70rem',
            80: '80rem',
            90: '90rem',
            100: '100rem'
        },

        fontFamily: {
            primary: ['Roboto', 'sans-serif'],
            secondary: ['Roboto', 'sans-serif']
        },

        fontSize: {
            xs: [
                '1.4rem',
                {
                    lineHeight: '1.5'
                }
            ],

            sm: [
                '1.6rem',
                {
                    lineHeight: '1.5'
                }
            ],

            base: [
                '1.8rem',
                {
                    lineHeight: '1.5'
                }
            ],

            lg: [
                '2rem',
                {
                    lineHeight: '1.4'
                }
            ],

            xl: [
                '2.4rem',
                {
                    lineHeight: '1.3'
                }
            ],

            '2xl': [
                '3rem',
                {
                    lineHeight: '1.3'
                }
            ],

            '3xl': [
                '4rem',
                {
                    lineHeight: '1.25'
                }
            ],

            '4xl': [
                '5rem',
                {
                    lineHeight: '1.2'
                }
            ],

            '5xl': [
                '6rem',
                {
                    lineHeight: '1.15'
                }
            ],

            '6xl': [
                '7rem',
                {
                    lineHeight: '1.1'
                }
            ]
        },

        opacity: {
            0: '0',
            10: '0.1',
            20: '0.2',
            30: '0.3',
            40: '0.4',
            50: '0.5',
            70: '0.7',
            80: '0.8',
            90: '0.9',
            100: '1'
        },

        screens: {
            smallDesktop: [{ max: '1700px' }],
            laptop: [{ max: '1450px' }],
            largeTablet: [{ max: '1240px' }],
            tablet: [{ max: '1030px' }],
            mediumTablet: [{ max: '880px' }],
            smallTablet: [{ max: '760px' }],
            mobile: [{ max: '600px' }],
            smallMobile: [{ max: '359px' }],

            aboveSmallDesktop: [{ min: '1701px' }],
            aboveLaptop: [{ min: '1451px' }],
            aboveLargeTablet: [{ min: '1241px' }],
            aboveTablet: [{ min: '1031px' }],
            aboveMediumTablet: [{ min: '881px' }],
            aboveSmallTablet: [{ min: '761px' }],
            aboveMobile: [{ min: '601px' }],
            aboveSmallMobile: [{ min: '360px' }]
        }
    },
    variants: {},
    plugins: [
        plugin(function ({ addUtilities, addComponents }) {
            const newUtilities = {
                '.ab100': {
                    position: 'absolute',
                    top: '0',
                    left: '0',
                    width: '100%',
                    height: '100%'
                },

                '.objectFitCover': {
                    'object-fit': 'cover',
                    'font-family': '"object-fit: cover;"'
                },

                '.transition-fast': {
                    transition: '0.15s cubic-bezier(0.4, 0, 0.2, 1)'
                },

                '.transition-normal': {
                    transition: '0.25s cubic-bezier(0.4, 0, 0.2, 1)'
                },

                '.transition-slow': {
                    transition: '0.35s cubic-bezier(0.4, 0, 0.2, 1)'
                },

                '.transition-slower': {
                    transition: '0.45s cubic-bezier(0.4, 0, 0.2, 1)'
                }
            };

            const gutters = {
                xs: '1rem',
                sm: '2rem',
                base: '2.5rem',
                lg: '3.5rem',
                xl: '5rem',
                '2xl': '6rem'
            };

            const components = {};

            for (const size in gutters) {
                const xy = {
                    margin: `-${gutters[size]};`,
                    '> *': {
                        padding: `${gutters[size]};`
                    }
                };

                const y = {
                    'margin-top': `-${gutters[size]};`,
                    'margin-bottom': `-${gutters[size]};`,
                    '> *': {
                        'padding-top': `${gutters[size]};`,
                        'padding-bottom': `${gutters[size]};`
                    }
                };

                const x = {
                    'margin-left': `-${gutters[size]};`,
                    'margin-right': `-${gutters[size]};`,
                    '> *': {
                        'padding-left': `${gutters[size]};`,
                        'padding-right': `${gutters[size]};`
                    }
                };

                components[`.gutter-${size}`] = xy;
                components[`.gutter-y-${size}`] = y;
                components[`.gutter-x-${size}`] = x;
            }

            addComponents(components, {
                variants: ['responsive']
            });

            addUtilities(newUtilities, {
                variants: ['responsive']
            });
        })
    ],
    purge: false
};
