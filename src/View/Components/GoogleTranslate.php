<?php

namespace Bravissimo\BladeGoogleTranslateComponent\View\Components;

use Illuminate\Support\Facades\Facade;
use Illuminate\View\Component as ViewComponent;

class GoogleTranslate extends ViewComponent {
    protected $path = 'views.googleTranslate';

    public $pageLanguage;
    public $includedLanguages;
    protected $ignoredLanguages;
    protected $languages;

    public function __construct(string $pageLanguage, ?array $includedLanguages = null, ?array $ignoredLanguages = []) {
        $this->pageLanguage = $pageLanguage;
        $this->ignoredLanguages = $ignoredLanguages;
        $this->includedLanguages = $includedLanguages;

        $this->languages = json_decode(file_get_contents(__DIR__ . '../../../../lang.json'), true);

        if (! empty($includedLanguages)) {
            $this->languages = array_filter($this->languages, function ($lang) {
                return in_array($lang, $this->includedLanguages);
            }, ARRAY_FILTER_USE_KEY);
        }
    }

    public function getLanguages() {
        return array_filter($this->languages, function ($lang) {
            return ! in_array($lang, $this->ignoredLanguages);
        }, ARRAY_FILTER_USE_KEY);
    }

    public function getLanguage($lang) {
        return $this->languages[$lang];
    }

    public function render() {
        $app = Facade::getFacadeApplication();

        $view = $app->get('view');

        $props = $this->extractPublicProperties();

        return $view->make($this->path, $props);
    }
}
