<?php

namespace Bravissimo\BladeGoogleTranslateComponent;

use Bravissimo\BladeGoogleTranslateComponent\View\Components\GoogleTranslate;

class BladeGoogleTranslateComponentServiceProvider {
    public function __construct() {
        $views = config('app.view_paths');
        $views[] = get_template_directory() . '/vendor/bravissimo_agency/blade-google-translate-component/resources';

        app('config')->set('app.view_paths', $views);
    }

    public function boot() {
        addBladeComponents([
            'googleTranslate' => GoogleTranslate::class,
            'views.googleTranslateButton' => 'googleTranslateButton',
        ]);
    }
}
