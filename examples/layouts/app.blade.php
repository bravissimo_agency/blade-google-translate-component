<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>blade-accordion-component</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        @if (!isHmrMode())
            <link rel="stylesheet" href="{{ mix('app.css') }}">
            <script src="{{ mix('app.js') }}" defer></script>
        @else
            <script src="{{ mix('app.js') }}" defer></script>
        @endif
    </head>

    <body>
        <div id="app" {{ $attributes }}>
            {!! $slot !!}
        </div>
    </body>
</html>
