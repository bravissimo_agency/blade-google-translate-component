import Vue from 'vue';

import Components from '../dist/index.js';

Vue.use(Components);

window.addEventListener('DOMContentLoaded', () => {
    // eslint-disable-next-line
    new Vue({
        el: '#app',

        methods: {
            log (value = 'test') {
                console.log(value);
            }
        }
    });
});
