<x-app class="p-10 flex flex-wrap">
    <div class="w-1/2 p-1">
        <x-googleTranslate 
            page-language="en"
            :included-languages="['en', 'sv', 'it']"
            :ignored-languages="['ceb', 'ny', 'ka', 'fy', 'ha', 'haw', 'ig', 'la', 'kk', 'mi', 'xh', 'si', 'or', 'lo', 'hmn', 'yo', 'te']"
            @changed="log"   
        >
            @foreach ($component->getLanguages() as $key => $language)
                <x-googleTranslateButton
                    :lang-code="$key"
                    class="googleTranslateButton skiptranslate"
                >
                    <img
                        src="/resources/assets/{{ $key }}.png"
                        style="width: 18px; height: 12px;"
                    >

                    {{ $language }}
                </x-googleTranslateButton>
            @endforeach
        </x-googleTranslate> 
    </div>

    <p>
        MY text
    </p>
</x-app>