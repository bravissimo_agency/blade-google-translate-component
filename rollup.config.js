import vue from 'rollup-plugin-vue';

export default [
    {
        input: 'resources/js/index.js',
        output: {
            format: 'es',
            dir: 'dist'
        },
        plugins: [vue()]
    }
];
